
package ependu;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class FichierNExisteException extends Exception{
    public FichierNExisteException(){
        super();
    }
    
    public void errorFrame(JFrame frame){
        JOptionPane.showMessageDialog(frame,"Error : Selected file doesn't exist","Error",JOptionPane.WARNING_MESSAGE);
    }
}
