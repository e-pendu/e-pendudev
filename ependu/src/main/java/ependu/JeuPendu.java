/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ependu;

/**
 *
 * @author samia
 */
public class JeuPendu {
    private int nbEssais;
    
    public JeuPendu() {
        nbEssais = 20; 
    }
    
    public void afficherEssais() {
        StringBuilder visuel = new StringBuilder();
        for (int i = 0; i < nbEssais; i++) {
            visuel.append("*"); 
        }
        System.out.println("Essais restants : " + visuel.toString());
        System.out.println("Nombre d'essais restants : " + nbEssais + "/ 20");
    }

    public void reduireEssais() {
        nbEssais--;
    }
    
    /*
private JLabel lblVisuel;
private JLabel lblNbEssais;

public InterfaceJeuPendu() {
    setTitle("Jeu du Pendu");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    lblVisuel = new JLabel();
    lblNbEssais = new JLabel();

    setLayout(new FlowLayout());
    add(lblVisuel);
    add(lblNbEssais);

    afficherEssais();

    pack();
    setLocationRelativeTo(null);
    setVisible(true);
}*/
}