
package ependu;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MauvaiseExtensionException extends Exception{
    public MauvaiseExtensionException(){
        super();
    }
    
    public void errorFrame(JFrame frame){
        JOptionPane.showMessageDialog(frame,"Error : Wrong file extension format","Error",JOptionPane.WARNING_MESSAGE);
    }
}
