package ependu;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author hugom, RicardDA, lorica, samia
 */
public class Ependu {
    static Dimension tailleEcran = Toolkit.getDefaultToolkit().getScreenSize();
    protected static ArrayList<String> motdisponible=null;
    protected static int nbvie=-1;
    protected static File fichiermot=null;
    static String[] listecarautorise={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
            "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
        "à","â","á","ä","ã","À","Â","Á","Ä","Ã","è","ê","é","ë","È","Ê","É","Ë","ì","î","í","ï","Ì","Î","Í","Ï","ò","ô","ó","ö","õ",
        "Ò","Ô","Ó","Ö","Õ","ù","û","ú","ü","Ù","Û","Ú","Ü","ñ","Ñ","ç","Ç"," ","-","'"};
    
    public static FenetreAcceuil premPage;
    
        public static void main(String[] args) {
        premPage = new FenetreAcceuil();
        premPage.setVisible(true);
        
        /*JeuPendu jeu = new JeuPendu();
        jeu.afficherEssais();

        for (int i = 0; i < 4; i++) {
            jeu.reduireEssais();
            jeu.afficherEssais();
        }*/
    }
    
    protected static ArrayList<String> lireListeMots(File fichier){
        ArrayList<String> motdisponible=new ArrayList<String>();
        Scanner pointeur=null;
        //FileReader fichierlecteur=null;
        Reader fichierlecteur=null; 
        String ligne=null;
        String[] lignecoupe;
        final int lenmaxmot=25;

        try{
            fichierlecteur= new InputStreamReader(new FileInputStream(fichier), "ISO8859_1");
            
            pointeur=new Scanner(fichierlecteur);

            while (pointeur.hasNextLine()){
                ligne=pointeur.nextLine();
                ligne=ligne.replaceAll(System.lineSeparator(),"");
                ligne=ligne.replaceAll("[)!.]","");
                try{
                    if(ligne.length()>lenmaxmot){
                        throw new MotLongException(ligne);
                    }
                    else{
                        lignecoupe=ligne.split("");
                        for(int iterateur=0;iterateur<lignecoupe.length;iterateur++){
                            if(!contient(listecarautorise,lignecoupe[iterateur])){
                                throw new CarInvalideException(ligne,lignecoupe[iterateur]);
                            }
                        }
                        motdisponible.add(ligne);
                    }
                }
                catch( MotLongException mle){
                    mle.errorFrame(null);
                }
                catch( CarInvalideException cie){
                    cie.errorFrame(null);
                }
            }
        }
        catch(FileNotFoundException fnte){
            System.out.println("Le fichier est introuvable.");
            JOptionPane.showMessageDialog(null,"Erreur : Le fichier est introuvable.","Erreur",JOptionPane.WARNING_MESSAGE);
        } 
        catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Ependu.class.getName()).log(Level.SEVERE, null, ex);
        }
        return motdisponible;
    } 
    
    private static boolean contient(String[] listecar,String carverif){
        boolean trouve=false;
        for(int i = 0 ; i<listecar.length &&trouve==false;i++){
            if(carverif.equals(listecar[i])){
                trouve=true;
            }
        }
        return trouve;
    }

    protected static File selectfichiermot(JFrame frame)throws AnnulSelection{
        File fichiermot=null;
        boolean valid=false;
        
        while (fichiermot==null || valid!=true){
            try{
                JFileChooser filechooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichier texte (\".txt\")","txt");

                Action details = filechooser.getActionMap().get("viewTypeDetails");
                details.actionPerformed(null);
                
                filechooser.setFileFilter(filter);
                filechooser.setAcceptAllFileFilterUsed(false);
                filechooser.setVisible(true);
                int longueur=tailleEcran.width * 1/2;
                int largeur=tailleEcran.height * 1/2;
                filechooser.setPreferredSize(new Dimension(longueur,largeur));

                filechooser.setDialogTitle("Sélectionner la liste de mot");
                filechooser.setDialogType(JFileChooser.OPEN_DIALOG);
                int valider=filechooser.showOpenDialog(frame);

                if(valider == JFileChooser.APPROVE_OPTION) {
                    fichiermot = filechooser.getSelectedFile();
                 
                    if(!fichiermot.exists()){
                        throw new FichierNExisteException();
                    }
                    String extension = fichiermot.getName();
                    extension = extension.substring(extension.lastIndexOf('.')+1);
                    extension = extension.replaceAll(String.valueOf('\n'),"");
                    if(!extension.contentEquals("txt")){
                        throw new MauvaiseExtensionException();
                    }
                    valid = true;
                    System.out.println("You chose to open this file: " + fichiermot.getName());
                    JOptionPane.showMessageDialog(filechooser,"File selected successfuly","Success",JOptionPane.INFORMATION_MESSAGE);
                }
                else if( valider==JFileChooser.CANCEL_OPTION){
                    throw new AnnulSelection();
                }   
            }
            catch(FichierNExisteException fnee){
                fnee.errorFrame(frame);
            }
            catch(MauvaiseExtensionException mee){
                mee.errorFrame(frame);
            }
        }
        return fichiermot;
    }

    
    
    protected static void ajoutmotliste(ArrayList<String> listemot,String nmot){
        try{
            String[] nmotcoupe=nmot.split("");
            for(int iterateur=0;iterateur<nmotcoupe.length;iterateur++){
                if(!contient(listecarautorise,nmotcoupe[iterateur])){
                    throw new CarInvalideException(nmot,nmotcoupe[iterateur]);
                }
            }
            listemot.add(nmot);
        }
        catch(CarInvalideException cie){
            cie.errorFrame(null);
        }
    }
}