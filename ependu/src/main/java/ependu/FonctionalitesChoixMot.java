package ependu;

import java.util.List;

public class FonctionalitesChoixMot {
    private static int genererNombreAleatoire(int valeurMin,int valeurMax){
        int valeurAleatoire = (int)Math.round(Math.random() * (valeurMax - valeurMin));
        return valeurAleatoire;
    }

    public String choisirMotAleatoire(List<String> listeMots){
        int tailleListe = listeMots.size();
        if(tailleListe > 0){
            int indice = genererNombreAleatoire(0, tailleListe - 1);
            String motADeviner = listeMots.get(indice);
            return motADeviner;
        }
        else{
            return null;
        }
    }
}