/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ependu;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author hugom
 */
public class CarInvalideException extends Exception{
    String mot;
    String carinvalide;
    public CarInvalideException(String mot,String carinvalide){
        super();
        this.mot=mot;
        this.carinvalide=carinvalide;
    }
    
    public void errorFrame(JFrame frame){
        JOptionPane.showMessageDialog(frame,"Erreur : Le caractère "+carinvalide+" du mot "+mot+" est invalide.","Erreur",JOptionPane.WARNING_MESSAGE);
    }
}
