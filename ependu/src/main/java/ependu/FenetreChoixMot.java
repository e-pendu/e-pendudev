package ependu;
import static ependu.Ependu.fichiermot;
import static ependu.Ependu.motdisponible;
import static ependu.Ependu.nbvie;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FenetreChoixMot extends JDialog{
    
    public FenetreChoixMot(JFrame fenetreParente){
        super(fenetreParente, "E-pendu", true);
        this.setSize(400, 300);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setContentPane(this.creerPanel());
        setLocationRelativeTo(null);
    }

    JPanel panel, panelJComboBox, panelCentre;
    JLabel labelChoixDifficulte, labelChoixMotPersonnalise,labelAjouterMot;
    JComboBox<String> choixDifficulte;
    JButton boutonValider, boutonAjouterMot, boutonOuvrirFichier;

    public JPanel creerPanel(){
        JPanel panel = (JPanel)getContentPane();
        panel.setLayout(new BorderLayout(20, 20));

        panelCentre = new JPanel();
        panelCentre.setLayout(new BoxLayout(panelCentre, BoxLayout.Y_AXIS));
        panelCentre.setAlignmentX(JPanel.CENTER_ALIGNMENT);

        labelChoixMotPersonnalise = new JLabel("Ouverture d'une liste de mots");
        labelChoixMotPersonnalise.setAlignmentX(JPanel.CENTER_ALIGNMENT);
        panelCentre.add(labelChoixMotPersonnalise);

        boutonOuvrirFichier = new JButton("Ouvrir un fichier");
        boutonOuvrirFichier.setAlignmentX(JPanel.CENTER_ALIGNMENT);
        boutonOuvrirFichier.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try{
                    fichiermot=Ependu.selectfichiermot(null);
                    motdisponible=Ependu.lireListeMots(fichiermot);
                    boutonAjouterMot.setEnabled(true);
                    choixDifficulte.setEnabled(true);
                    boutonValider.setEnabled(true);
                } catch (AnnulSelection ex) {
                    
                }
            }
        }); //TODO
        panelCentre.add(boutonOuvrirFichier);

        labelAjouterMot = new JLabel("Ajouter un mot");
        labelAjouterMot.setAlignmentX(JPanel.CENTER_ALIGNMENT);
        panelCentre.add(labelAjouterMot);

        boutonAjouterMot = new JButton("Ajouter");
        boutonAjouterMot.setEnabled(false);
        boutonAjouterMot.setAlignmentX(JPanel.CENTER_ALIGNMENT);
        boutonAjouterMot.addActionListener((ActionEvent e) -> {
            FenetreEntreeTexte fenetreajout=new FenetreEntreeTexte(this);
            
        }); //TODO
        panelCentre.add(boutonAjouterMot);

        labelChoixDifficulte = new JLabel("Choisissez le niveau de difficulté de votre choix");
        labelChoixDifficulte.setAlignmentX(JPanel.CENTER_ALIGNMENT);
        panelCentre.add(labelChoixDifficulte, BorderLayout.NORTH);

        panelJComboBox = new JPanel();
        panelJComboBox.setLayout(new FlowLayout());

        choixDifficulte = new JComboBox<String>();
        choixDifficulte.addItem("Facile");
        choixDifficulte.addItem("Normal");
        choixDifficulte.addItem("Difficile");
        choixDifficulte.setEnabled(false);
        panelJComboBox.add(choixDifficulte);

        JPanel sud = new JPanel();
        sud.setBackground(Color.WHITE);
        sud.setLayout(new FlowLayout());

        panelCentre.add(panelJComboBox);

        panel.add(panelCentre);

        boutonValider = new JButton("Jouer");
        boutonValider.setPreferredSize(new Dimension(150, 20));
        boutonValider.addActionListener((ActionEvent e) -> {
            String itemselectionne=(String)choixDifficulte.getSelectedItem();
            switch(itemselectionne){
                case "Facile":
                    nbvie=17;
                    break;
                case "Normal":
                    nbvie=12;
                    break;
                case "Difficile":
                    nbvie=7;
                    break;
            }
            this.dispose();
            InterfacePartie partie=new InterfacePartie();
        });

        boutonValider.setEnabled(false);
        
        sud.add(boutonValider);
        
        getContentPane().add(sud, BorderLayout.SOUTH);

        return panel;
    }
}