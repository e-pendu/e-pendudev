
package ependu;

import static ependu.Ependu.motdisponible;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author hugom
 */
public class FenetreEntreeTexte extends JDialog{
    static Dimension tailleEcran = Toolkit.getDefaultToolkit().getScreenSize();
    public FenetreEntreeTexte(JDialog fenetreparente){
        super(fenetreparente,true);
        setSize(tailleEcran.width/4,tailleEcran.height/6);
        setTitle("Entrez un nouveau mot");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new BoxLayout(getContentPane(),BoxLayout.Y_AXIS));
        setResizable(false);
        this.setLocationRelativeTo(null);
        
        JTextField textfield = new JTextField();
        textfield.setColumns(30);
        
        JPanel paneltexte=new JPanel();
        JPanel panelbouton=new JPanel();
        JButton ok=new JButton("Ok"),annuler=new JButton("Annuler");
        
        ok.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                String entree=textfield.getText();
                if(entree.isEmpty()==false){
                    Ependu.ajoutmotliste(motdisponible, entree);
                    dispose();
                }
            }
        });
        annuler.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                dispose();
            }
        });
        
        getContentPane().add(Box.createVerticalGlue());
        getContentPane().add(paneltexte);
        getContentPane().add(panelbouton);
        getContentPane().add(Box.createVerticalGlue());
        
        panelbouton.add(ok);
        panelbouton.add(annuler);
        
        paneltexte.add(textfield);
        paneltexte.setLayout(new FlowLayout());
        
        setVisible(true);
    }
}
