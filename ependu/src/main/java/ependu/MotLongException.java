/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ependu;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author hugom
 */
public class MotLongException extends Exception{
    String mot;
    public MotLongException(String mot){
        super();
        this.mot=mot;
    }
    
    public void errorFrame(JFrame frame){
        JOptionPane.showMessageDialog(frame,"Erreur : Le mot "+mot+" est trop long","Erreur",JOptionPane.WARNING_MESSAGE);
    }
}
