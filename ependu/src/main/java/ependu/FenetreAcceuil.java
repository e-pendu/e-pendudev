/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ependu;

import java.awt.*;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author samia
 */
public class FenetreAcceuil extends JFrame{
    
    private JButton jouerButton;
    private JLabel titreLabel;
    

    public FenetreAcceuil() {
        setTitle("Bienvenue au jeu du pendu");
        setMinimumSize(new Dimension(470, 520));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setIconImage(new ImageIcon("logo_page.png").getImage());
        setLocationRelativeTo(null);
        setResizable(false);
        
        JMenuBar menuBar = new JMenuBar();
        JMenu fichierMenu = new JMenu("Menu");
        JMenuItem commencerMenuItem = new JMenuItem("Commencer");
        JMenuItem sortirMenuItem = new JMenuItem("Sortir");

        commencerMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FenetreChoixMot difficulte = new FenetreChoixMot(null);
                difficulte.setVisible(true);
            }
        });

        sortirMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        fichierMenu.add(commencerMenuItem);
        fichierMenu.addSeparator();
        fichierMenu.add(sortirMenuItem);
        menuBar.add(fichierMenu);
        setJMenuBar(menuBar);

        JPanel nord = new JPanel();
        nord.setBackground(Color.WHITE);
        nord.setLayout(new FlowLayout());
        JPanel sud = new JPanel();
        sud.setBackground(Color.WHITE);
        sud.setLayout(new FlowLayout());
        JPanel ouest = new JPanel();
        ouest.setBackground(Color.WHITE);
        ouest.setLayout(new FlowLayout());
        
        titreLabel = new JLabel("E - Pendu");
        titreLabel.setFont(new Font("Arial", Font.BOLD, 24));
        titreLabel.setHorizontalAlignment(SwingConstants.CENTER);
        titreLabel.setAlignmentX(CENTER_ALIGNMENT);
        nord.add(titreLabel);

        jouerButton = new JButton("Commencer");
        jouerButton.setFont(new Font("Arial", Font.PLAIN, 18));
        jouerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FenetreChoixMot difficulte = new FenetreChoixMot(null);
                difficulte.setVisible(true);
            }
        });
        jouerButton.setAlignmentX(CENTER_ALIGNMENT);
        sud.add(jouerButton);
        
        
        ImageIcon imageIcon = new ImageIcon(new ImageIcon("logo.png").getImage().getScaledInstance(446, 447, Image.SCALE_DEFAULT));

        JLabel imageLabel = new JLabel();
        imageLabel.setIcon(imageIcon);
        imageLabel.setAlignmentX(CENTER_ALIGNMENT);
        ouest.add(imageLabel);
        
        getContentPane().add(nord, BorderLayout.NORTH);
        getContentPane().add(ouest, BorderLayout.WEST);
        getContentPane().add(sud, BorderLayout.SOUTH);
        
        setLocationRelativeTo(null); 
        setVisible(true);
    }
}
