package ependu;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;
/**
 *
 * @author p2203228
 */
public class InterfacePartie extends JFrame implements ActionListener{
    static Dimension tailleEcran = Toolkit.getDefaultToolkit().getScreenSize();
    
    public InterfacePartie(){
        super(" La partie commence !");
        RoundBtn arrondir = new RoundBtn(15);
        int longueur = tailleEcran.width * 1/12;
        int hauteur = tailleEcran.height * 1/7;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setIconImage(new ImageIcon("logo_page.png").getImage());
        setSize(longueur * 6,hauteur * 7/2);
        setLocationRelativeTo(null);
        
        ImageIcon image = new ImageIcon(new ImageIcon("logo.png").getImage().getScaledInstance(longueur, hauteur, Image.SCALE_DEFAULT));
        JLabel logo = new JLabel();
        logo.setIcon(image);
        this.add(logo, BorderLayout.NORTH);
        JButton a = new JButton("A");
        a.setBorder((Border) arrondir);
        a.addActionListener(this);
        JButton b = new JButton("B"); 
        b.setBorder((Border) arrondir);
        b.addActionListener(this);
        JButton c = new JButton("C"); 
        c.setBorder((Border) arrondir);
        c.addActionListener(this);
        JButton d = new JButton("D"); 
        d.setBorder((Border) arrondir);
        d.addActionListener(this);
        JButton e = new JButton("E"); 
        e.addActionListener(this);
        e.setBorder((Border) arrondir);
        JButton f = new JButton("F"); 
        f.addActionListener(this);
        f.setBorder((Border) arrondir);
        JButton g = new JButton("G"); 
        g.addActionListener(this);
        g.setBorder((Border) arrondir);
        JButton h = new JButton("H"); 
        h.addActionListener(this);
        h.setBorder((Border) arrondir);
        JButton i = new JButton("I"); 
        i.addActionListener(this);
        i.setBorder((Border) arrondir);
        JButton j = new JButton("J"); 
        j.addActionListener(this);
        j.setBorder((Border) arrondir);
        JButton k = new JButton("K"); 
        k.addActionListener(this);
        k.setBorder((Border) arrondir);
        JButton l = new JButton("L"); 
        l.addActionListener(this);
        l.setBorder((Border) arrondir);
        JButton m = new JButton("M"); 
        m.addActionListener(this);
        m.setBorder((Border) arrondir);
        JButton n = new JButton("N"); 
        n.addActionListener(this);
        n.setBorder((Border) arrondir);
        JButton o = new JButton("O"); 
        o.addActionListener(this);
        o.setBorder((Border) arrondir);
        JButton p = new JButton("P"); 
        p.addActionListener(this);
        p.setBorder((Border) arrondir);
        JButton q = new JButton("Q"); 
        q.addActionListener(this);
        q.setBorder((Border) arrondir);
        JButton r = new JButton("R"); 
        r.addActionListener(this);
        r.setBorder((Border) arrondir);
        JButton s = new JButton("S"); 
        s.addActionListener(this);
        s.setBorder((Border) arrondir);
        JButton t = new JButton("T"); 
        t.addActionListener(this);
        t.setBorder((Border) arrondir);
        JButton u = new JButton("U"); 
        u.addActionListener(this);
        u.setBorder((Border) arrondir);
        JButton v = new JButton("V"); 
        v.addActionListener(this);
        v.setBorder((Border) arrondir);
        JButton w = new JButton("W"); 
        w.addActionListener(this);
        w.setBorder((Border) arrondir);
        JButton x = new JButton("X"); 
        x.addActionListener(this);
        x.setBorder((Border) arrondir);
        JButton y = new JButton("Y"); 
        y.addActionListener(this);
        y.setBorder((Border) arrondir);
        JButton z = new JButton("Z"); 
        z.setBorder((Border) arrondir);
        z.addActionListener(this);
        
        // ne touche pas à ça stp
        JLabel lab = new JLabel("JEAN EUDE");
        lab.setPreferredSize(new Dimension (200,200));
        lab.setAlignmentX(LEFT_ALIGNMENT);
        lab.setAlignmentY(CENTER_ALIGNMENT);
        JLabel labe = new JLabel();
        labe.setPreferredSize(new Dimension (200,200));
        this.add(lab, BorderLayout.EAST);
        this.add(labe, BorderLayout.WEST);
        // ne pas toucher 
        
        // ici tu peux toucher
        JPanel panelbtn = new JPanel(new FlowLayout());
        panelbtn.setAlignmentX(CENTER_ALIGNMENT);
        panelbtn.add(a);
        panelbtn.add(b);
        panelbtn.add(c);
        panelbtn.add(d);
        panelbtn.add(e);
        panelbtn.add(f);
        panelbtn.add(g);
        panelbtn.add(h);
        panelbtn.add(i);
        panelbtn.add(j);
        panelbtn.add(k);
        panelbtn.add(l);
        panelbtn.add(m);
        panelbtn.add(n);
        panelbtn.add(o);
        panelbtn.add(p);
        panelbtn.add(q);
        panelbtn.add(r);
        panelbtn.add(s);
        panelbtn.add(t);
        panelbtn.add(u);
        panelbtn.add(v);
        panelbtn.add(w);
        panelbtn.add(x);
        panelbtn.add(y);
        panelbtn.add(z);
        this.add(panelbtn, BorderLayout.CENTER);
        getContentPane().setBackground(Color.WHITE);
        panelbtn.setBackground(Color.WHITE);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(false){
            //Facultatif, sauf si vous voulez ajouter d'autres boutons
        }
        else{
            JButton boutonATraiter = (JButton) e.getSource();
            boutonATraiter.setEnabled(false);
            //TODO : Récupérer la lettre avec : boutonATraiter.getText()
        }
        
    }
    
    private class RoundBtn implements Border {
        private int r;
    RoundBtn(int r) {
        this.r = r;
    }
    public Insets getBorderInsets(Component c) {
        return new Insets(this.r+1, this.r+1, this.r+2, this.r);
    }
    public boolean isBorderOpaque() {
        return true;
    }
    public void paintBorder(Component c, Graphics g, int x, int y,int width, int height) {
        g.drawRoundRect(x, y, width-1, height-1, r, r);
        }
    }
}