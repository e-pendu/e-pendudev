**E-pendu**
=======

Description
=======
E-pendu est un projet realise dans le cadre du cours de qualite developpement du BUT Informatique. E-pendu reprends le principe du jeu du pendu et l'adapte au format numerique. Le but est de trouver le mot choisi par l'ordinateur en demandant quelles lettres sont presentes dans le mot grace aux boutons des lettres de l'alphabet. 3 modes de difficulte sont disponibles : facile,normal,difficile. Chaque difficulte vous offre un nombre d'essai plus ou moins haut pour trouver la solution.

Installation
=======
Les dossiers sont disponibles sur le [depot git](https://forge.univ-lyon1.fr/e-pendu/e-pendudev) du projet E-pendu.

Pour installer le jeu, plusieurs options sont a votre disposition.

1.Archives compressees
-------
Vous pouvez installer le projet sous la forme d'un dossier compresse zip ou tar depuis le [depot git](https://forge.univ-lyon1.fr/e-pendu/e-pendudev). Il vous suffit ensuite de le decompresser pour pouvoir lancer le jeu.

2.Clone depuis Git
-------
Vous pouvez installer le projet en utilisant la commande git ci dessous apres avoir ouvert l'invite de commande git dans votre dossier de destination. *Cette manipulation necessite d'avoir installe git sur votre machine.*
```git
git clone https://forge.univ-lyon1.fr/e-pendu/e-pendudev.git
```

Conditions d'utilisation
=======
L'utilisation de E-pendu est réservée à un usage personnel.
La copie ou la modification de l'application à des fins commerciales est possible uniquement à condition de mentionner explicitement les auteurs originaux de l'application.


Auteurs
=======
 - ABBAS Sami 
 - AUDIN Loric
 - DIAS ARANTES Ricardo
 - MAYRAND Hugo https://forge.univ-lyon1.fr/p2206917

